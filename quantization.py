# Quantization
# Eager Mode Python API




# Quantized Tensors
#### torch.quantize_per_tensor
#### torch.quantize_per_channel

# Quantized Modules
#### torch.nn.quantized
#### torch.nn.quantized.dynamic

# Quantization tools
#### torch.quantization
#### torch.quantization.Observer
#### torch.quantization.FakeQuantize









# Dynamic Quantization
# conv weights to int8, conv acti on the fly and compute,
# activations read and writen in float

import torch.quantization
quantized_model = torch.quantization.quantize_dynamic(
    model,
    {torch.nn.Linear},
    dtype=torch.qint8
)




# Post-Training Static Quantization
#### Novelties
######## Observers
######## Operator fusion -> fusing several operations into a single one
######## Per-channel quantization


# set quantization config for server (x86)
deploymentMyModel.qconfig = torch.quantization.get_default_qconfig('fbgemm')

# insert observers
torch.quantization.prepare( myModel, inplace=True )
# calibrates models and collects statistics

# converrt to quantized version
torch.quantization.convert( myModel, inplace=True )












# Quantization Aware Training



# specify quantization config for QAT
qat_model.qconfig = torch.quantization.get_default_qat_qconfig('fbgemm')

# prepare QAT
torch.quantization.prepare_qat(qat_model, inplace=True)

# convert to quantized version, removing dropout, to check for accuracy on each
epochquantized_model = torch.quantization.convert(qat_model.eval(), inplace=False)


# fake quantization during training, floating points emulate int8 values,
# but operations are still performed on floating point numbers.
# weights are "aware that they will be quantized later after train ->
# achieves maximum quantization pos training


# line 78 - inserts fake quantization modules to model quantization
# line 80 - mimics static quantization, quantizes the model once training is complete




# Quantization is not yet available for multiple devices
# Available operators depend on the backend used
# CPU inference on x86 and ARM
# Backend dependet

import torchbackend='fbgemm'
# fbgemm for server, qnnpack for mobile
my_model.config = torch.quantization.get_default_qconfig( backend )

torch.backends.quantized.engine = backend