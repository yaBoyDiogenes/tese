def binary_matrix_to_csv( lst, name='aux.csv' ) :

    handler = open( name, 'w' )

    for i, j in lst :
        handler.write(
            '%d, %0.3f\n' % ( i, j )
        )


# Wrong
def classes_acc_to_csv( table, classes, name='classes.csv' ) :

    l = len( table )
    handler = open( name, 'w' )

    for c in range( len(classes ) ) :

        handler.write( classes[ c ] + '\n' )

        for i in range( l ) :
            
            print( table )

            handler.write(
                '%d, %0.3f\n' % ( i, table[i][c] )
            )