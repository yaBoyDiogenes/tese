import torch.nn as nn

# Here, the parameters expansion should be named shrinkage
class ResidualBlock( nn.Module ) :
    def __init__( self, in_channels, out_channels, stride=1, expansion_factor=4 ) :

        super( ResidualBlock, self ).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.stride = stride
        self.expansion_factor = expansion_factor

        self.bn1 = nn.BatchNorm2d( in_channels )
        self.relu1 = nn.ReLU( inplace = True )
        self.conv1 = nn.Conv2d(
            in_channels,
            in_channels // expansion_factor,
            kernel_size = (1,1),
            stride = 1,
            bias = False
        )

        self.bn2 = nn.BatchNorm2d( in_channels // expansion_factor )
        self.relu2 = nn.ReLU( inplace = True )
        self.conv2 = nn.Conv2d(
            in_channels // expansion_factor,
            in_channels // expansion_factor,
            kernel_size = (3,3),
            stride = stride,
            padding = 1,
            groups = in_channels // expansion_factor,
            bias = False
        )

        self.bn3 = nn.BatchNorm2d( in_channels // expansion_factor )
        self.relu3 = nn.ReLU( inplace = True )
        self.conv3 = nn.Conv2d(
            in_channels // expansion_factor,
            out_channels,
            kernel_size = (1,1),
            stride = 1,
            bias = False
        )

        self.shortcut = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size = (1,1),
            stride = stride,
            bias = False
        )

    
    def forward( self, x ) :

        residual = x

        out = self.conv1( self.relu1( self.bn1( x ) ) )
        out = self.conv2( self.relu2( self.bn2( out ) ) )
        out = self.conv3( self.relu3( self.bn3( out ) ) )

        if ( self.in_channels != self.out_channels ) or ( self.stride != 1 ) :
            residual = self.shortcut( self.bn1( residual ) )

        out += residual

        return out




class InvertedResidual( nn.Module ) :
    def __init__(
        self,
        in_channels,
        out_channels,
        stride = 1,
        expansion_factor = 4, #6
        apply_shortcut = True
    ) :

        super( InvertedResidual, self ).__init__()

        self.apply_shortcut = apply_shortcut

        self.shortcut = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size = (1,1),
            stride = stride,
            bias = False
        )

        self.bn1 = nn.BatchNorm2d( in_channels )
        self.relu1 = nn.ReLU6( inplace = True )
        self.conv1_pointwise = nn.Conv2d(
            in_channels,
            in_channels * expansion_factor,
            kernel_size = (1,1),
            stride = 1,
            bias = False
        )

        self.bn2 = nn.BatchNorm2d(
            in_channels * expansion_factor
        )
        self.relu2 = nn.ReLU6( inplace = True )
        self.conv2_depthwise = nn.Conv2d(
            in_channels * expansion_factor,
            in_channels * expansion_factor,
            kernel_size = (3,3),
            stride = stride,
            padding = 1,
            groups = in_channels * expansion_factor,
            bias = False
        )

        self.bn3 = nn.BatchNorm2d(
            in_channels * expansion_factor
        )
        self.relu3 = nn.ReLU6( inplace = True )
        self.conv3_pointwise = nn.Conv2d(
            in_channels * expansion_factor,
            out_channels,
            kernel_size = (1,1),
            stride = 1,
            bias = False
        )


    def forward( self, x ) :

        out = self.bn1( x )
        out = self.relu1( out )
        out = self.conv1_pointwise( out )
        out = self.bn2( out )
        out = self.relu2( out )
        out = self.conv2_depthwise( out )
        out = self.bn3( out )
        out = self.relu3( out )
        out = self.conv3_pointwise( out )

        if self.apply_shortcut :
            residual = self.shortcut( x )
            out += residual

        return out




class SeparableConvolution( nn.Module ) :
    def __init__( self, in_channels, out_channels, stride=1 ) :

        super( SeparableConvolution, self ).__init__()

        self.bn = nn.BatchNorm2d( in_channels )
        self.relu = nn.ReLU( inplace = True )
        self.conv_depth = nn.Conv2d(
            in_channels,
            in_channels,
            kernel_size = (3,3),
            stride = stride,
            padding = 1,
            groups=in_channels,
            bias = False,
        )
        self.conv_point = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size = (1,1),
            bias = False
        )

    def forward( self, x ) :

        out = self.bn( x )
        out = self.relu( out )
        out = self.conv_depth( out )
        out = self.conv_point( out )

        return out
