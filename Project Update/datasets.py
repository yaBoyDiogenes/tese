from numpy.random import choice
from torchvision import transforms, datasets

import torch

class CIFAR10 :


    classes = (
        'plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck'
    )

    trans = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize( (0.5, 0.5, 0.5), (0.5, 0.5, 0.5) )
    ])

    trans_data_aug = transforms.Compose([
        transforms.RandomCrop(32, padding=4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ])


    def __init__( self, batch_size=64, data_aug=True, shuffle=True, percent=1.0 ) :

        self.train_loader = self.load_train( batch_size, data_aug, shuffle, percent )
        self.test_loader = self.load_test( batch_size, shuffle, percent )


    def load_train( self, batch_size=64, data_aug=True, shuffle=True, percent=1.0 ) :

        trainset = datasets.CIFAR10(
            root = '.data',
            train = True,
            download = True,
            transform = self.trans_data_aug if data_aug else self.trans
        )

        trainset = torch.utils.data.Subset(
            trainset,
            choice(
                [ idx for idx in range( len( trainset ) ) ],
                size = int( percent * len( trainset ))
            )
        )

        trainloader = torch.utils.data.DataLoader(
            trainset,
            batch_size = batch_size,
            shuffle = shuffle,
            num_workers = 2
        )

        return trainloader


    def load_test( self, batch_size=64, shuffle=True, percent=1.0 ) :

        testset = datasets.CIFAR10(
            root = '.data',
            train = False,
            download = True,
            transform = self.trans
        )

        testset = torch.utils.data.Subset(
            testset,
            choice(
                [ idx for idx in range( len( testset ) ) ],
                size = int( percent * len( testset ))
            )
        )

        testloader = torch.utils.data.DataLoader(
            testset,
            batch_size = batch_size,
            shuffle = shuffle,
            num_workers = 2
        )

        return testloader