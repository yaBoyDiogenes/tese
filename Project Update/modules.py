from blocks import ResidualBlock

import torch.nn as nn

class Attention_Module_Stage1( nn.Module ) :
    def __init__( self, channels, size1=(16,16), size2=(8,8) ) :


        super( Attention_Module_Stage1, self ).__init__()


        self.first = ResidualBlock(
            channels,
            channels
        )

        
        self.trunk = nn.Sequential(
            ResidualBlock(
                channels,
                channels
            ),
            ResidualBlock(
                channels,
                channels
            )
        )

        
        self.max1 = nn.MaxPool2d(
            kernel_size = (3,3),
            stride = 2,
            padding = 1
        )
        self.max1_pos = ResidualBlock(
            channels,
            channels
        )
        self.max1_shortcut = ResidualBlock(
            channels,
            channels
        )
        self.max2 = nn.MaxPool2d(
            kernel_size = (3,3),
            stride = 2,
            padding = 1,
        )
        self.max2_pos = nn.Sequential(
            ResidualBlock(
                channels,
                channels
            ),
            ResidualBlock(
                channels,
                channels
            )
        )
        self.iter1 = nn.UpsamplingBilinear2d(
            size = size2
        )
        self.iter1_pos_after_shortcut = ResidualBlock(
            channels,
            channels
        )
        self.iter2 = nn.UpsamplingBilinear2d(
            size = size1
        )

        self.conv_blocks = nn.Sequential(
            nn.BatchNorm2d( channels ),
            nn.ReLU( inplace = True ),
            nn.Conv2d(
                channels,
                channels,
                kernel_size = 1,
                stride = 1,
                bias = False
            ),
            nn.BatchNorm2d( channels ),
            nn.ReLU( inplace = True ),
            nn.Conv2d(
                channels,
                channels,
                kernel_size = 1,
                stride = 1,
                bias = False
            ),
            nn.Sigmoid()
        )

        self.last = ResidualBlock(
            channels,
            channels
        )


    def forward( self, x ) :

        x = self.first( x )

        trunk = self.trunk( x )

        out = self.max1( x )
        x = self.max1_pos( out )
        out_shortcut = self.max1_shortcut( out )
        out = self.max2( x )
        out = self.max2_pos( out )
        out = self.iter1( out ) + out_shortcut
        out += x
        out = self.iter2( out ) + trunk
        out = self.conv_blocks( out )
        out = ( 1 + out ) * trunk
        out = self.last( out )

        return out


class Attention_Module_Stage2( nn.Module ) :
    def __init__( self, channels, size=(8,8) ) :
        super( Attention_Module_Stage2, self ).__init__()

        self.first = ResidualBlock(
            channels,
            channels
        )

        self.trunk = nn.Sequential(
            ResidualBlock(
                channels,
                channels
            ),
            ResidualBlock(
                channels,
                channels
            )
        )

        self.max1 = nn.MaxPool2d(
            kernel_size = (3,3),
            stride = 1,
            padding = 1
        )

        self.max1_pos = nn.Sequential(
            ResidualBlock(
                channels,
                channels
            )
        )

        self.iter1 = nn.UpsamplingBilinear2d( size=size )

        self.conv_blocks = nn.Sequential(
            nn.BatchNorm2d( channels ),
            nn.ReLU( inplace = True ),
            nn.Conv2d(
                channels,
                channels,
                kernel_size = (1,1),
                stride = 1,
                bias = False
            ),
            nn.BatchNorm2d( channels ),
            nn.ReLU( inplace = True ),
            nn.Conv2d(
                channels,
                channels,
                kernel_size = (1,1),
                stride = 1,
                bias = False
            ),
            nn.Sigmoid()
        )

        self.last = ResidualBlock(
            channels,
            channels
        )


    def forward( self, x ) :

        x = self.first( x )

        trunk = self.trunk( x )

        out = self.max1( x )
        out = self.max1_pos( out )
        out = self.iter1( out ) + trunk

        out = self.conv_blocks( out )

        out = ( 1 + out ) * trunk

        out = self.last( x )

        return out


class Attention_Module_Stage3( nn.Module ) :
    def __init__( self, channels ) :
        super( Attention_Module_Stage3, self ).__init__()

        self.first = ResidualBlock(
            channels,
            channels
        )

        self.trunk = nn.Sequential(
            ResidualBlock(
                channels,
                channels
            ),
            ResidualBlock(
                channels,
                channels
            )
        )

        self.mid = nn.Sequential(
            ResidualBlock(
                channels,
                channels
            ),
            ResidualBlock(
                channels,
                channels
            )
        )

        self.conv_blocks = nn.Sequential(
            nn.BatchNorm2d( channels ),
            nn.ReLU( inplace = True ),
            nn.Conv2d(
                channels,
                channels,
                kernel_size = (1,1),
                stride = (1,1),
                bias = False
            ),
            nn.BatchNorm2d( channels ),
            nn.ReLU( inplace = True ),
            nn.Conv2d(
                channels,
                channels,
                kernel_size = (1,1),
                stride = (1,1),
                bias = False
            ),
            nn.Sigmoid()
        )

        self.last = ResidualBlock(
            channels,
            channels
        )

    
    def forward( self, x ) :

        x = self.first( x )

        trunk = self.trunk( x )

        out = self.mid( x )
        out = self.conv_blocks( out )

        out = ( 1 + out ) * trunk

        out = self.last( out )

        return out