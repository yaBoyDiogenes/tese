from blocks import ResidualBlock
from modules import Attention_Module_Stage1
from modules import Attention_Module_Stage2
from modules import Attention_Module_Stage3

import torch.nn as nn

class ResidualAttentionNet( nn.Module ) :
    def __init__( self ) :
        
        super( ResidualAttentionNet, self ).__init__()

        self.conv1 = nn.Sequential(
            nn.Conv2d(
                3,
                32,
                kernel_size = 3,
                stride = 1,
                padding = 1,
                bias = False
            ),
            nn.BatchNorm2d( 32 ),
            nn.ReLU( inplace = True )
        )

        self.res1 = ResidualBlock( 32, 128 )
        self.att1 = Attention_Module_Stage1( 128, size1=(32,32), size2=(16,16) )

        self.res2 = ResidualBlock( 128, 256, 2 )
        self.att20 = Attention_Module_Stage2( 256, size=(16,16) )
        self.att21 = Attention_Module_Stage2( 256, size=(16,16) )

        self.res3 = ResidualBlock( 256, 512, 2 )
        self.att30 = Attention_Module_Stage3( 512 )
        self.att31 = Attention_Module_Stage3( 512 )
        self.att32 = Attention_Module_Stage3( 512 )

        self.res4 = ResidualBlock( 512, 1024)
        self.res5 = ResidualBlock( 1024, 1024)
        self.res6 = ResidualBlock( 1024, 1024)

        self.mpool = nn.Sequential(
            nn.BatchNorm2d( 1024 ),
            nn.ReLU( inplace = True ),
            nn.AvgPool2d( kernel_size = 8 )
        )

        self.fc = nn.Linear( 1024, 10 )


    def forward( self, x ) :

        out = self.conv1( x )
        out = self.res1( out )
        out = self.att1( out )
        out = self.res2( out )
        out = self.att20( out )
        out = self.att21( out )
        out = self.res3( out )
        out = self.att30( out )
        out = self.att31( out )
        out = self.att32( out )
        out = self.res4( out )
        out = self.res5( out )
        out = self.res6( out )
        out = self.mpool( out )
        out = self.fc( out.view( out.size( 0 ), -1 ) )

        return out