from termcolor import colored

import time
import torch
import blocks
import seaborn as sns
import torch.nn as nn
import matplotlib.pyplot as plt


sns.set_theme()


device = torch.device( 'cuda' if torch.cuda.is_available() else 'cpu' )




def parameters_count( model, display=True, color='yellow' ) :

    params = sum( p.numel() for p in model.parameters() )

    params_trainable = sum( p.numel() for p in model.parameters() if p.requires_grad )

    params_not_trainable = sum( p.numel() for p in model.parameters() if not p.requires_grad )

    print( colored( 'Parameters: %d' % params, color )  )
    print( colored( 'Trainable Parameters: %d' % params_trainable, color )  )
    print( colored( 'Non Trainable Parameters: %d' % params_not_trainable, color )  )

    return params, params_trainable, params_not_trainable




def time_count( model, inpt, display=True, color='blue', rnd=5  ) :

    x = time.time()

    _ = model( inpt )

    x = time.time() - x

    print( colored( 'Elapsed Time: %0.5f'% x , color ) )

    return round( x, rnd )







# Convolution Types

## Parameters when doubling up the filters

parameters = []
times = []

for x in [ 32, 64, 128, 256, 512, 1024, 2048 ] :

    normal = nn.Conv2d(
        x,
        x * 2,
        kernel_size = (3,3),
        padding = 1,
        bias = False
    ).to( device )

    sep = blocks.SeparableConvolution(
        x,
        x * 2
    ).to( device )


    pn, _, _ = parameters_count( normal )
    psep, _, _ = parameters_count( sep )

    parameters.append((x, pn, psep))


    data = torch.ones((64,x,256,256)).to( device )

    xn = time_count( normal, data )
    xsep = time_count( sep, data )

    times.append((x, xn, xsep))




plt.title( 'Number of Parameters' )

plt.plot(
    [ x for x, _, _ in parameters ],
    [ y for _, y, _ in parameters ],
    label="Standard 3x3 Convolution"
)

plt.plot(
    [ x for x, _, _ in parameters ],
    [ z for _, _, z in parameters ],
    label="Separable Convolution"
)

plt.xlabel( 'Inicial Filter' )
plt.ylabel( 'Number of Total Parameters' )

plt.legend()

plt.savefig( 'parameters.jpeg' )




plt.clf()




plt.title( 'Elapsed Time' )

plt.plot(
    [ x for x, _, _ in times ],
    [ y for _, y, _ in times ],
    label="Standard 3x3 Convolution"
)

plt.plot(
    [ x for x, _, _ in times ],
    [ z for _, _, z in times ],
    label="Separable Convolution"
)

plt.xlabel( 'Inicial Filter' )
plt.ylabel( 'Time Spent (s)' )

plt.legend()

plt.savefig( 'times.jpeg' )