from __future__ import print_function, division


from torch.autograd import Variable

from termcolor import colored

import torch
import torch.nn as nn
import torch.optim as optim


from datasets import CIFAR10
from models import ResidualAttentionNet
from utils import binary_matrix_to_csv as d2csv
from utils import classes_acc_to_csv as clscsv


from google.colab import drive
drive.mount( '/content/gdrive' )




NUM_CLASSES = 10
acc_lst = []
cls_acc_lst = []
loss_lst = []




def test( model, test_ldr, classes, epoch, batch_size=64 ) :

    correct, total = 0, 0
    correct_lst, total_lst = [0. for _ in range(NUM_CLASSES)], [0. for _ in range(NUM_CLASSES)]

    for imgs, lbls in test_ldr :

        imgs, lbls = imgs.to( device ), lbls.to( device )

        outputs = model( imgs )

        _, predicted = torch.max( outputs.data, 1 )

        total += imgs.size( 0 )
        correct += ( predicted == lbls.data ).sum()

        c = ( predicted == lbls.data ).squeeze()

        for i, _ in enumerate( lbls, 0 ) :

            lbl = lbls.data[ i ]
            correct_lst[ lbl ] += c[ i ]
            total_lst[ lbl ] += 1

    acc_lst.append(( epoch, float( correct ) / total ))
    cls_acc_lst.append(
            [ float( x ) / y for x, y in list( zip( correct_lst, total_lst ) ) ]
    )

    print( colored( 'Acc: %0.3f' % acc_lst[ -1][1], 'green' ) )
    for i in range( NUM_CLASSES ) :
        print( colored( '%s: %0.3f' % ( classes[ i ], cls_acc_lst[-1][ i ] ), 'blue' ) )




device = torch.device(
    'cuda' if torch.cuda.is_available() else 'cpu'
)




lr = 0.1
epochs = 150
dataset = CIFAR10( 64 )



model = ResidualAttentionNet().to( device )

crit = nn.CrossEntropyLoss()

opt = optim.SGD(
    model.parameters(),
    lr=lr,
    momentum=0.9,
    nesterov=True,
    weight_decay=0.0001
)


acc, best = 0, 0


for epoch in range( epochs ) :

    print( colored( "Epoch %d" % epoch, 'white' ) )

    running_loss = 0.0


    for i, (imgs, lbls) in enumerate( dataset.train_loader ) :

        imgs, lbls = imgs.to( device ), lbls.to( device )

        opt.zero_grad()

        outputs = model( imgs )
        loss = crit( outputs, lbls )

        loss.backward()
        opt.step()

        running_loss += loss.item()


        if i % 200 == 199 :

            acc += 200

            loss_lst.append( ( acc, running_loss / 200 ) )
            
            print( colored( 'Loss: %d, %0.3f' % ( loss_lst[-1] ), 'cyan' ) )

            running_loss = 0.0

    

    test( model, dataset.test_loader, dataset.classes, epoch, 64 )

    


    if epoch in [ 40, 80, 120 ] :

        lr /= 10

        for param_group in opt.param_groups :
            print( param_group['lr'] ) 
            param_group['lr'] = lr
            print( param_group['lr'] ) 

        print( colored( 'Reseting LR: %0.5f' % lr, 'red' ) )




    if acc_lst[ -1 ][1] > best and acc_lst[ -1 ][1] > 0.875 :

        name = 'resAtt' + str( acc_lst[ -1 ][1] ) + '.pt'
        path = F"/content/gdrive/MyDrive/Tese/{name}"
        torch.save( model.state_dict(), path )




d2csv( acc_lst, '/content/gdrive/MyDrive/Tese/acc.csv' )
d2csv( loss_lst, '/content/gdrive/MyDrive/Tese/loss.csv' )
# clscsv( cls_acc_lst, '/content/gdrive/MyDrive/Tese/classes_acc.csv' )
