import torch
from torch import nn
import torch.nn.utils.prune as prune
import torch.nn.functional as F




# device = torch.device(
#     'cuda' if torch.cuda.is_available() else 'cpu'
# )

device = torch.device( 'cpu' )

class LeNet(nn.Module):
    def __init__(self):
        super(LeNet, self).__init__()
        # 1 input image channel, 6 output channels, 3x3 square conv kernel
        self.conv1 = nn.Conv2d(1, 6, 3)
        self.conv2 = nn.Conv2d(6, 16, 3)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)  # 5x5 image dimension
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1, int(x.nelement() / x.shape[0]))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

model = LeNet().to( device=device )

module = model.conv1
'''
print(list(module.named_parameters()))

print(list(module.named_buffers()))


# Subclass of Base Pruning Method

prune.random_unstructured(
    module,
    name = 'weight',
    amount = 0.3
)

print( '-' * 8 )
print(list(module.named_parameters()))
print(list(module.named_buffers()))

print(module.weight)



# This module don't delete the weights, instead they use a
# masks, named weight_mask

print( module._forward_pre_hooks )

prune.l1_unstructured(
    module,
    name='bias',
    amount=3
)


print(list(module.named_parameters()))

print( '\n' * 10 )

print(list(module.named_buffers()))

print( '\n' * 10 )

print( module.bias )

print( '\n' * 10 )

print(module._forward_pre_hooks)


'''







# Interactive Pruning

prune.ln_structured(
    module,
    name='weight',
    amount=0.5,
    n=2,
    dim=0
)

print( module.weight )
# Masks on Mask

for hook in module._forward_pre_hooks.values() :
    if hook._tensor_name == 'weight' :
        break
    print(list(hook))






# Serializing a pruned model
print(model.state_dict().keys())




# Remove pruning re-parametrization
    # Making it permanent
print(list(module.named_parameters()))
print(list(module.named_buffers()))

prune.remove(module, 'weight')
print(list(module.named_parameters()))





# Pruning multiple parameters in a model
new = LeNet()

for name, module in new.named_modules() :
    if isinstance( module, torch.nn.Conv2d ) :
        prune.l1_unstructured(
            module,
            name='weight',
            amount=0.2
        )
    elif isinstance( module, torch.nn.Linear ) :
        prune.l1_unstructured(
            module,
            name='weight',
            amount=0.4
        )

print(
    dict(
        new.named_parameters()
    ).keys()
)

print(
    dict(
        new.named_buffers()
    ).keys()
)



# comparing statistic as weight magniteude, actiavation, gradient
# above and locally



# now
# global prunning
# remove from the whole model instead


model = LeNet()

parameters_to_prune = (
    (model.conv1, 'weight'),
    (model.conv2, 'weight'),
    (model.fc1, 'weight'),
    (model.fc2, 'weight'),
    (model.fc3, 'weight'),
)

prune.global_unstructured(
    parameters_to_prune,
    pruning_method=prune.L1Unstructured,
    amount=0.2
)


print(
    'Sparsity on the conv1 layers %0.03f' %
    (100. * float(torch.sum(model.conv1.weight==0))
    / float(model.conv1.weight.nelement()))
)

print(
    'Sparsity on the fc1 layers %0.03f' %
    (100. * float(torch.sum(model.fc1.weight==0))
    / float(model.fc1.weight.nelement()))
)



class FooBarPruningMethod( prune.BasePruningMethod ) :
    PRUNING_TYPE = 'unstructured'

    def compute_mask( self, t, default_mask ) :
        mask = default_mask.clone()
        mask.view(-1)[::2] = 0
        return mask

def foobar_unstructured( module, name ) :
    FooBarPruningMethod.apply( module, name )
    return module


model = LeNet()
foobar_unstructured(model.fc3, name='bias')
print( model.fc3.bias_mask )