
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader, sampler

from Models.UNet import UNet
from Utils.Utils import to_pil, numpy_to_pil
from Datasets.COCO_utils import COCO


import os
import torch
from argparse
import torch.nn as nn
import matplotlib.pyplot as plt


import numpy as np
from PIL import Image

from pathlib import Path




parser = argparse.ArgumentParser(description='Training UNet on COCO2017')




parser.add_argument( '--dir', type=str )
parser.add_argument( '--train', type=int )
parser.add_argument( '--epochs', type=int )
parser.add_argument( '--eval', type=int )
parser.add_argument( '--lr', type=float )
parser.add_argument( '--weight', type=float )
parser.add_argument( '--momentum', type=float )




parser.set_defaults( dir = '/home/luispereira/Desktop/COCOdataset2017' )
parser.set_defaults( train = 1 )
parser.set_defaults( epochs = 100 )
parser.set_defaults( eval = 1 )
parser.set_defaults( lr = 0.001 )
parser.set_defaults( weights = 0.001 )
parser.set_defaults( momentum = 0.9 )




args = parser.parse_args()




base = '/home/luispereira/Desktop/COCOdataset2017'
classes = [
    'bus',
    'person',
    'laptop'
]




# TODO: Add Image Augmentation
t = transforms.Compose([
    transforms.ToTensor()
])




if args.train :
    train = COCO(
        dir = base,
        classes = classes,
        mode = 'val',
        transform = t,
    )

    train_dl = DataLoader(
        train,
        batch_size=8,
        shuffle=True
    )




if args.val :
    val = COCO(
        dir = base,
        classes = classes,
        mode = 'val',
        transform = t
    )

    val_dl = DataLoader(
        val,
        batch_size=64,
        shuffle=True,
    )




device = torch.device(
    'cuda' if torch.cuda.is_available() else 'cpu'
)




model_path = 'Best_Models'

if model_path not in os.listdir() :
    os.mkdir( model_path )


images_path = 'Images'

if images_path not in os.listdir() :
    os.mkdir( images_path )




model = UNet( 3, len( classes ) + 1 ).to( device )

lr_decay = [ 30, 60, 90 ]

crit = nn.CrossEntropyLoss()

optimizer = torch.optim.SGD(
    model.parameters(),
    lr=args.lr,
    nesterov=True,
    momentum=args.momentum,
    weight_decay=args.weight
)



best_acc, i = 0, 0




# Add More Metrics, Acc is not the best for this project
def acc_fun( output, mask ) :
    return ( output == mask ).float().mean()




# Create a function on utils, for now this is good enough
def print_and_save( original, ground_truth, output ) :


    original = np.stack([
            original[0],
            original[1],
            original[2],
    ], axis = 2 ) * 256
    original = Image.fromarray( original.astype( np.uint8 ), 'RGB' )


    mask = np.stack([
            mask * (256 / len( classes ) + 1),
            mask * (256 / len( classes ) + 1),
            mask * (256 / len( classes ) + 1),
    ], axis = 2 ) * 256
    mask = Image.fromarray( mask.astype( np.uint8 ), 'RGB' )


    predicted = np.stack([
            predicted * (256 / len( classes ) + 1),
            predicted * (256 / len( classes ) + 1),
            predicted * (256 / len( classes ) + 1),
    ], axis = 2 )
    predicted = Image.fromarray( predicted.astype( np.uint8 ), 'RGB' )


    fig, ax = plt.subplots( 1, 3, figsize=(10, 9))
    ax[ 0 ].imshow( original )
    ax[ 1 ].imshow( mask )
    ax[ 2 ].imshow( predicted )


    plt.savefig( Path( images_path ) / ( 'img%d.jpg' % i ) )








if args.train :


    for epoch in range( args.epochs ) :


        if epoch in lr_decay :
            lr /= 10


            for param_group in optimizer.param_groups :
                param_group['lr'] = lr


        for i, ( imgs, masks ) in enumerate( train_dl, 0 ) :


            imgs = imgs.to( device )
            masks = masks.to( device ).squeeze(1).type( torch.int64 )


            optimizer.zero_grad()
            outputs = model( imgs )


            loss = crit( outputs, masks )
            loss.backward()

            optimizer.step()


            acc = acc_fun( outputs.argmax(1), masks )


            if epoch in range( 0, 100, 5 ) + [ args.epochs ] :

                torch.save(
                    model.state_dict(),
                    ( Path( model_path ) / ( 'mod%d.pt' % epoch ) )
                )
        



            if i % 25 == 0 :
                print( '[Step %d : %0.3f]' % ( i, loss.item() ) )

                print_and_save(
                    imgs[0].detach().cpu(),
                    masks[0].detach().cpu(),
                    outputs[0].argmax(dim=0).detach().cpu()
                )


if args.val :
    pass