from Models.UNet import UNet
from Models.UNet_Light import UNet_Light
from Models.UNet_Light2 import UNet_Light2

from pathlib import Path
from Datasets.COCO_utils import COCO

import os
from PIL import Image
import numpy as np
import seaborn as sns
import matplotlib.pylab as plt

import torch

from torchvision import transforms
from torch.utils.data import Dataset, DataLoader, sampler

sns.set_style("dark")


SANDBOX_DIR = 'Sandbox'

if SANDBOX_DIR not in os.listdir() :
    os.mkdir( SANDBOX_DIR )

SANDBOX = Path( SANDBOX_DIR )




UNET_PATH = 'Best_Models/mod22.pt'
UNET_LIGHT_PATH = 'Best_Models/mod8_light.pt'
UNET_LIGHT2_PATH = None




base = '/home/luispereira/Desktop/COCOdataset2017'
classes = [
    'person',
    'bicycle',
    'car',
    'bus',
    'bird',
    'cat',
    'dog',
    'bottle',
    'cup',
    'laptop',
]




model_unet = UNet( 3, len( classes ) + 1 )
model_unet.load_state_dict( torch.load( UNET_PATH ) )


model_unet_light = UNet_Light( 3, len( classes ) + 1 )
model_unet_light.load_state_dict( torch.load( UNET_LIGHT_PATH ) )


model_unet_light2 = UNet_Light2( 3, len( classes ) + 1 )






def count_params( model ) :
    p, ptrain, pnontrain = 0, 0, 0
    for param in model.parameters() :
        p += param.numel()
        if param.requires_grad :
            ptrain += param.numel()
        else :
            pnontrain += param.numel()
    return p, ptrain, pnontrain







x1, _, _ = count_params( model_unet )
x2, _, _ = count_params( model_unet_light )
x3, _, _ = count_params( model_unet_light2 )


print( x2 / float(x1) )
print( x3 / float(x1) )


x = ['UNet', 'UNet Light', 'UNet Light2']
y = [x1 / x1, x2 / float(x1) , x3 / float(x1)]


plt.bar( x, y )
# plt.show()




t = transforms.Compose([
    transforms.ToTensor()
])

val = COCO(
    dir = base,
    classes = classes,
    mode = 'val',
    transform = t
)

val_dl = DataLoader(
    val,
    batch_size=4,
    shuffle=True,
)



device = torch.device( 'cuda' )


model_unet = model_unet.to( device )
model_unet_light = model_unet_light.to( device )
# model_unet_light2 = model_unet_light2.to( device )


for img, lbl in val_dl :

    img = img.to( device )
    
    output1 = model_unet( img )
    output2 = model_unet_light( img )
    # output3 = model_unet_light2( img )



    break


output1 = output1[0].argmax(dim=0).detach().cpu()
output2 = output2[0].argmax(dim=0).detach().cpu()
# output3 = output2[0].argmax(dim=0).detach().cpu()





original = np.stack([
        img.detach().cpu()[0][0],
        img.detach().cpu()[0][1],
        img.detach().cpu()[0][2],
], axis = 2 ) * 256
original = Image.fromarray( original.astype( np.uint8 ), 'RGB' )


mask = np.stack([
        lbl[0][0].detach().cpu() * ( 256.0 / (len( classes ) + 1)),
        lbl[0][0].detach().cpu() * ( 256.0 / (len( classes ) + 1)),
        lbl[0][0].detach().cpu() * ( 256.0 / (len( classes ) + 1)),
], axis = 2 )
mask = Image.fromarray( mask.astype( np.uint8 ), 'RGB' )


output1 = np.stack([
        output1 * ( 256.0 / (len( classes ) + 1)),
        output1 * ( 256.0 / (len( classes ) + 1)),
        output1 * ( 256.0 / (len( classes ) + 1)),
], axis = 2 )
output1 = Image.fromarray( output1.astype( np.uint8 ), 'RGB' )


output2 = np.stack([
        output2 * ( 256.0 / (len( classes ) + 1)),
        output2 * ( 256.0 / (len( classes ) + 1)),
        output2 * ( 256.0 / (len( classes ) + 1)),
], axis = 2 )
output2 = Image.fromarray( output2.astype( np.uint8 ), 'RGB' )

# output3 = np.stack([
#         output3 * ( 256.0 / (len( classes ) + 1)),
#         output3 * ( 256.0 / (len( classes ) + 1)),
#         output3 * ( 256.0 / (len( classes ) + 1)),
# ], axis = 2 )
# output3 = Image.fromarray( output3.astype( np.uint8 ), 'RGB' )



fig, ax = plt.subplots( 2, 2, figsize=(10, 9))
# fig, ax = plt.subplots( 2, 3, figsize=(10, 9))
ax[ 0, 0 ].imshow( original )
ax[ 0, 1 ].imshow( mask )
ax[ 1, 0 ].imshow( output1 )
ax[ 1, 1 ].imshow( output2 )
# ax[ 1, 2 ].imshow( output3 )


plt.show()