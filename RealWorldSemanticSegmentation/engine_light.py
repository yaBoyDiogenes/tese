
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader, sampler

from Models.UNet_Light import UNet_Light
from Utils.Utils import to_pil, numpy_to_pil
from Datasets.COCO_utils import COCO


import os
import torch
import argparse
import torch.nn as nn
import matplotlib.pyplot as plt


import numpy as np
from PIL import Image

from pathlib import Path




parser = argparse.ArgumentParser(description='Training UNet on COCO2017')




# TODO: Add checkpoints and resume training
parser.add_argument( '--dir', type=str )
parser.add_argument( '--train', type=int )
parser.add_argument( '--epochs', type=int )
parser.add_argument( '--eval', type=int )
parser.add_argument( '--lr', type=float )
parser.add_argument( '--weight', type=float )
parser.add_argument( '--momentum', type=float )
parser.add_argument( '--resume', type=int )
parser.add_argument( '--checkpoint', type=str )
parser.add_argument( '--checkpoint_epoch', type=int )




parser.set_defaults( dir = '/home/luispereira/Desktop/COCOdataset2017' )
parser.set_defaults( train = 1 )
parser.set_defaults( epochs = 100 )
parser.set_defaults( eval = 1 )
parser.set_defaults( lr = 0.001 )
parser.set_defaults( weight = 0.001 )
parser.set_defaults( momentum = 0.9 )
parser.set_defaults( resume = 0 )
parser.set_defaults( checkpoint = 'None' )
parser.set_defaults( checkpoint_epoch = 0 )




# args = parser.parse_args(
#     '--resume 1 --checkpoint Best_Models/mod16.pt --checkpoint_epoch 16'.split()
# )
args = parser.parse_args()




base = '/home/luispereira/Desktop/COCOdataset2017'
classes = [
    'person',
    'bicycle',
    'car',
    'bus',
    'bird',
    'cat',
    'dog',
    'bottle',
    'cup',
    'laptop',
]




# TODO: Add Image Augmentation and smarter data loading between the number
# TODO: of classes and its instances
t = transforms.Compose([
    transforms.ToTensor()
])




if args.train :
    train = COCO(
        dir = base,
        classes = classes,
        mode = 'train',
        # mode = 'val',
        transform = t,
    )

    train_dl = DataLoader(
        train,
        batch_size=6,
        shuffle=True
    )




if args.eval :
    val = COCO(
        dir = base,
        classes = classes,
        mode = 'val',
        transform = t
    )

    val_dl = DataLoader(
        val,
        batch_size=64,
        shuffle=True,
    )




device = torch.device(
    'cuda' if torch.cuda.is_available() else 'cpu'
)




model_path = 'Best_Models'

if model_path not in os.listdir() :
    os.mkdir( model_path )


images_path = 'Images'

if images_path not in os.listdir() :
    os.mkdir( images_path )




model = UNet_Light( 3, len( classes ) + 1 ).to( device )

if args.resume :
    model.load_state_dict(torch.load(args.checkpoint))
    starting_epoch = args.checkpoint_epoch + 1
else :
    starting_epoch = 0

lr_decay = [ 30, 60, 90 ]

crit = nn.CrossEntropyLoss()

optimizer = torch.optim.SGD(
    model.parameters(),
    lr=args.lr,
    nesterov=True,
    momentum=args.momentum,
    weight_decay=args.weight
)



best_acc, i = 0, 0




# TODO: Add More Metrics, Acc is not the best for this project
def acc_fun( output, mask ) :
    return ( output == mask ).float().mean()




# TODO: Create a function on utils, for now this is good enough
def print_and_save( original, mask, output ) :


    original = np.stack([
            original[0],
            original[1],
            original[2],
    ], axis = 2 ) * 256
    original = Image.fromarray( original.astype( np.uint8 ), 'RGB' )


    mask = np.stack([
            mask * ( 256.0 / (len( classes ) + 1)),
            mask * ( 256.0 / (len( classes ) + 1)),
            mask * ( 256.0 / (len( classes ) + 1)),
    ], axis = 2 )
    mask = Image.fromarray( mask.astype( np.uint8 ), 'RGB' )


    output = np.stack([
            output * ( 256.0 / (len( classes ) + 1)),
            output * ( 256.0 / (len( classes ) + 1)),
            output * ( 256.0 / (len( classes ) + 1)),
    ], axis = 2 )
    output = Image.fromarray( output.astype( np.uint8 ), 'RGB' )


    fig, ax = plt.subplots( 1, 3, figsize=(10, 9))
    ax[ 0 ].imshow( original )
    ax[ 1 ].imshow( mask )
    ax[ 2 ].imshow( output )


    plt.savefig( Path( images_path ) / ( 'img%d.jpg' % i ) )








if args.train :


    for epoch in range( starting_epoch, args.epochs ) :


        print( "[Epoch %d]" % epoch )


        if epoch in lr_decay :
            lr /= 10


            for param_group in optimizer.param_groups :
                param_group['lr'] = lr


        for i, ( imgs, masks ) in enumerate( train_dl, 0 ) :


            imgs = imgs.to( device )
            masks = masks.to( device ).squeeze(1).type( torch.int64 )


            optimizer.zero_grad()
            outputs = model( imgs )


            loss = crit( outputs, masks )
            loss.backward()

            optimizer.step()


            acc = acc_fun( outputs.argmax(1), masks )




            if i % 50 == 0 :
                
                # FIXME: The value of the loss is wrong, since it doesnt take
                # FIXME: in account all the values, only for a given instance
                print( '[Step %d : %0.3f]' % ( i, loss.item() ) )

                print_and_save(
                    imgs[0].detach().cpu(),
                    masks[0].detach().cpu(),
                    outputs[0].argmax(dim=0).detach().cpu()
                )


        if epoch in range( 0, args.epochs + 1, 2 ) :

            torch.save(
                model.state_dict(),
                ( Path( model_path ) / ( 'mod%d_light.pt' % epoch ) )
            )




if args.eval :
    pass