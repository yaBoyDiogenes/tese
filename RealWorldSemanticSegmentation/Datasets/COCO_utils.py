from torch.utils.data import Dataset, DataLoader, sampler

from PIL import Image
from pathlib import Path
import matplotlib.pyplot as plt
from pycocotools.coco import COCO as cc

import torch.nn as nn

import cv2
import torch
import numpy as np




class COCO( Dataset ) :

    def __init__( self, dir='COCOdataset2017', classes=None, mode='train', shuffle=False, multi=True, transform=None ) :

        super().__init__()

        self.folder = Path( dir )
        self.classes = classes
        self.mode = mode # train or val
        self.shuffle = shuffle
        self.multi = multi

        self.images, self.size, self.cc = self.filter( mode, shuffle )
        self.classes_ids = self.get_classes_id( self.classes )

        self.transform = transform




    '''
    Filter is used to choose the significant images to the problem
    COCO dataset has 91 categories, by using filter, it is possible to choose
    only images that have at least one of the classes to the current problem
    '''
    def filter( self, mode='train', shuffle=False ) :

        anns = self.folder / 'annotations' / ('%s.json' % mode)
        coco = cc( anns )

        aux = []
        if self.classes != None :
            for c in self.classes :
                cat_ids = coco.getCatIds( catNms = c )
                img_ids = coco.getImgIds( catIds = cat_ids )
                aux += img_ids
        else :
            aux = coco.getImgIds()

        images = []
        for i in coco.loadImgs( aux ) :
            if i not in images : images.append( i )

        return images, len( images ), coco




    '''
    Auxiliary function
    Given the id, find the class of the item
    '''
    def get_class_name( self, class_id, cats ) :

        for i in range( len( cats ) ) :
            if cats[ i ][ 'id' ] == class_id :
                return cats[ i ][ 'name' ]
        return None




    '''
    Auxiliary function to get the name of the classes by giving a id
    '''
    def get_classes_id( self, classes_nms ) :

        ids_nms = []
        for c in classes_nms :
            try :
                ids_nms.append( self.cc.getCatIds( catNms=c )[0] )
            except :
                from termcolor import color
                import sys
                print( colored( 'Error importing some class', 'red' ) )
                sys.exit( 1 )

        return ids_nms




    '''
    Given the dictionary of the images and the dimensions,
    open the image
    '''
    def get_image( self, img_dict, dims=(192, 192) ) :

        img = Image.open(
            self.folder / 'images' / self.mode / img_dict[ 'file_name' ]
        ).resize( dims )

        if len( np.asarray( img ).shape ) != 3 :
            img = Image.fromarray(
                np.stack( (np.asarray( img ),) * 3, axis=2 )
            )

        return np.asarray( img )




    '''
    Creates the masks for the current problem
    The masks used for each images changes with the current problem,
    therefore, this apporach is needed, since the masks and the value of
    used to represent each class cheanges
    '''
    def get_normal_mask( self, img_dict, dims=(192, 192) ) :

        cat_ids = self.classes_ids.copy()

        ann_ids = self.cc.getAnnIds( img_dict['id'], catIds=cat_ids, iscrowd=None )
        anns = self.cc.loadAnns( ann_ids )
        cats = self.cc.loadCats( cat_ids )
        
        msk = np.zeros( dims )

        for a in range( len( anns ) ) :
            
            class_name = self.get_class_name(anns[a]['category_id'], cats)
            pixel_value = self.classes.index( class_name ) + 1

            new_mask = cv2.resize( self.cc.annToMask( anns[a] ) * pixel_value, dims )

            msk = np.maximum( new_mask, msk )

        return msk




    '''
    Function used to binary classificiation, as, background or other object
    '''
    def get_binary_mask( self, img_dict, dims=(192, 192), threshold=0.5 ) :

        cat_ids = self.classes_ids

        ann_ids = self.cc.getAnnIds( img_dict['id'], catIds=cat_ids, iscrowd=None )
        anns = self.cc.loadAnns( ann_ids )

        msk = np.zeros(dims)

        for a in range( len(anns) ) :

            nw = cv2.resize( self.cc.annToMask( anns[ a ] ), dims )

            nw[ nw >= 0.5 ] = 1
            nw[ nw < 0.5 ] = 0.5

            msk = np.maximum( nw, msk )

        return msk




    def __len__( self ) :
        return len( self.images )




    def __getitem__( self, id ) :

        x = self.get_image( self.images[ id ] )
        
        if self.multi :
            y = self.get_normal_mask( self.images[ id ] )
        else :
            y = self.get_binary_mask( self.images[ id ] )

        if self.transform :
            x = self.transform( x )
            y = self.transform( y )

        return x, y




    def __repr__( self ) :

        s = '%d' % ( self.__len__() )
        return s






if __name__ == '__main__' :

    '''
    Test the dataloader and use some visualization folders in order to
    assert that the functions and loaders are correctly written

    Also, first instance to use as interface showcase
    '''

    base = Path( 'COCOdataset2017' )

    data = COCO(
        classes = [ 'motorcycle', 'bus', 'truck' ],
        mode = 'val',
        multi=True
    )

    train_dl = DataLoader(
        data,
        batch_size=12,
        shuffle=True
    )

    print( len( train_dl ) )



    x = cc( Path( 'COCOdataset2017' ) / 'annotations' / 'val.json' )
    print( x.getCatIds() )


    i = 0
    for imgs, masks in train_dl :

        for j in range( len( imgs ) ) :
            i += 1
            fig, ax = plt.subplots( 1, 2, figsize=(10, 9))
            ax[ 0 ].imshow( imgs[j] )
            ax[ 1 ].imshow( masks[j] )


            plt.savefig('Yolo/something%d.jpg' % i)