import torch
import torch.nn as nn


class SeparableConv( nn.Module ) :
    def __init__( self, in_channels, out_channels ) :
        super( SeparableConv, self ).__init__()

        self.c1 = nn.Conv2d(
            in_channels,
            in_channels,
            kernel_size = (3,3),
            padding = 1,
            groups = in_channels,
            bias = False
        )
        self.c2 = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size = (1,1),
            bias = False
        )
        self.bn = nn.BatchNorm2d( out_channels )
        self.relu = nn.ReLU( inplace=True )

    def forward( self, x ) :

        out = self.c1( x )
        out = self.c2( out )
        out = self.bn( out )
        out = self.relu( out )

        return out




class BiConv( nn.Module ) :
    def __init__( self, channels ) :
        super( BiConv, self ).__init__()

        self.conv1 = SeparableConv(
            channels,
            channels
        )
        self.bn1 = nn.BatchNorm2d( channels )
        self.relu1 = nn.ReLU( inplace = True )

        self.conv2 = SeparableConv(
            channels,
            channels
        )
        self.bn2 = nn.BatchNorm2d( channels )
        self.relu2 = nn.ReLU( inplace = True )


    def forward( self, x ) :
        
        out = self.relu1( self.bn1( self.conv1( x ) ) )
        out = self.relu2( self.bn2( self.conv2( out ) ) )

        return out




class BiConvRedution( nn.Module ) :
    def __init__( self, in_channels, out_channels ) :
        super( BiConvRedution, self ).__init__()

        self.conv1 = SeparableConv( in_channels, out_channels )
        self.bn1 = nn.BatchNorm2d( out_channels )
        self.relu1 = nn.ReLU( inplace=True  )

        self.conv2 = SeparableConv( out_channels, out_channels )
        self.bn2 = nn.BatchNorm2d( out_channels )
        self.relu2 = nn.ReLU( inplace=True )


    def forward( self, x ) :

        out = self.conv1( x )
        out = self.bn1( out )
        out = self.relu1( out )
        out = self.conv2( out )
        out = self.bn2( out )
        out = self.relu2( out )

        return out




class Upsampling( nn.Module ) :
    def __init__( self, in_channels, out_channels, size ) :
        super( Upsampling, self ).__init__()

        self.up = nn.UpsamplingBilinear2d( size )
        self.conv = SeparableConv( in_channels, out_channels )

    def forward( self, x ) :
        out = self.up( x )
        out = self.conv( out )

        return out




class UNet_Light2( nn.Module ) :

    def __init__( self, in_channels, out_channels ) :

        super( UNet_Light2, self ).__init__()


        # Contrating
        self.conv1 = SeparableConv(
            in_channels,
            64
        )
        self.biconv1 = BiConv( 64 )
        self.max1 = nn.MaxPool2d((2,2))

        self .conv2 = SeparableConv(
            64,
            128
        )
        self.biconv2 = BiConv( 128 )
        self.max2 = nn.MaxPool2d((2,2))

        self.conv3 = SeparableConv(
            128,
            256
        )
        self.biconv3 = BiConv( 256 )
        self.max3 = nn.MaxPool2d((2,2))

        self.conv4 = SeparableConv(
            256,
            512
        )
        self.biconv4 = BiConv( 512 )
        self.max4 = nn.MaxPool2d((2,2))


        # Middle
        self.conv_mid = SeparableConv(
            512,1024
        )
        self.biconv_mid = BiConv( 1024 )
        self.up1 = Upsampling( 1024, 512, (38, 38) )


        # Expanding
        self.biconvred1 = BiConvRedution( 1024, 512 )
        self.up2 = Upsampling( 512, 256, (76, 76) )

        self.biconvred2 = BiConvRedution( 512, 256 )
        self.up3 = Upsampling( 256, 128, (152, 152) )


        self.biconvred3 = BiConvRedution( 256, 128 )
        self.up4 = Upsampling( 128, 64, (304, 304) )

        self.biconvred4 = BiConvRedution( 128, 64 )
        self.conv_final = SeparableConv(
            64,
            out_channels
        )


    def forward( self, x ) :

        # Contracting
        out = self.conv1( x )
        s1 = self.biconv1( out )
        out = self.max1( s1 )

        out = self.conv2( out )
        s2 = self.biconv2( out )
        out = self.max2( s2 )

        out = self.conv3( out )
        s3 = self.biconv3( out )
        out = self.max3( s3 )

        out = self.conv4( out )
        s4 = self.biconv4( out )
        out = self.max4( s4 )


        # Mid
        out = self.conv_mid( out )
        out = self.biconv_mid( out )
        out = self.up1( out )


        # Expand
        out = torch.cat([out, s4], 1)
        out = self.biconvred1( out )
        out = self.up2( out )
        out = torch.cat([out,s3], 1)
        out = self.biconvred2( out )
        out = self.up3( out )

        out = torch.cat([out,s2], 1)
        out = self.biconvred3( out )
        out = self.up4( out )

        out = torch.cat([out,s1], 1)
        out = self.biconvred4( out )
        out = self.conv_final( out )

        return out




if __name__ == '__main__' :

    import torch

    x1 = UNet_Light2( 3, 3 )    
    y = torch.ones((4,3,304,304))


    print( x1( y ).shape )