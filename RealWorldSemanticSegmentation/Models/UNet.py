import torch
import torch.nn as nn




'''
TODO: Change the part with the loop and sequentials, to
proportionate a better pruning
'''




class BiConv( nn.Module ) :
    def __init__( self, channels, repetition ) :
        super( BiConv, self ).__init__()

        self.repetition = repetition

        self.arm = nn.Sequential(
            nn.Conv2d(
                channels,
                channels,
                kernel_size = (3,3),
                padding = 1,
                bias = False
            ),
            nn.BatchNorm2d( channels ),
            nn.ReLU( inplace=True )
        )


    def forward( self, x ) :
        
        for _ in range( self.repetition ) :
            x = self.arm( x )

        return x




class BiConvRedution( nn.Module ) :
    def __init__( self, in_channels, out_channels ) :
        super( BiConvRedution, self ).__init__()

        self.conv1 = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size = (3,3),
            padding = 1,
            bias = False
        )
        self.bn1 = nn.BatchNorm2d( out_channels )
        self.relu1 = nn.ReLU( inplace=True  )

        self.conv2 = nn.Conv2d(
            out_channels,
            out_channels,
            kernel_size = (3,3),
            padding = 1,
            bias = False
        )
        self.bn2 = nn.BatchNorm2d( out_channels )
        self.relu2 = nn.ReLU( inplace=True )


    def forward( self, x ) :

        out = self.conv1( x )
        out = self.bn1( out )
        out = self.relu1( out )
        out = self.conv2( out )
        out = self.bn2( out )
        out = self.relu2( out )

        return out




class UNet( nn.Module ) :

    def __init__( self, in_channels, out_channels ) :

        super( UNet, self ).__init__()


        # Contrating
        self.conv1 = nn.Conv2d(
            in_channels,
            64,
            kernel_size = (3,3),
            padding = 1,
            bias = False
        )
        self.biconv1 = BiConv( 64, 2 )
        self.max1 = nn.MaxPool2d((2,2))

        self.conv2 = nn.Conv2d(
            64,
            128,
            kernel_size = (3,3),
            padding = 1,
            bias = False
        )
        self.biconv2 = BiConv( 128, 2 )
        self.max2 = nn.MaxPool2d((2,2))

        self.conv3 = nn.Conv2d(
            128,
            256,
            kernel_size = (3,3),
            padding = 1,
            bias = False
        )
        self.biconv3 = BiConv( 256, 2 )
        self.max3 = nn.MaxPool2d((2,2))

        self.conv4 = nn.Conv2d(
            256,
            512,
            kernel_size = (3,3),
            padding = 1,
            bias = False
        )
        self.biconv4 = BiConv( 512, 2 )
        self.max4 = nn.MaxPool2d((2,2))


        # Middle
        self.conv_mid = nn.Conv2d(
            512,
            1024,
            kernel_size = (3,3),
            padding = 1,
            bias = False
        )
        self.biconv_mid = BiConv( 1024, 2 )
        self.up1 = nn.ConvTranspose2d(
            1024,
            512,
            kernel_size = (2,2),
            stride = (2,2),
            bias = False
        )


        # Expanding
        self.biconvred1 = BiConvRedution( 1024, 512 )
        self.up2 = nn.ConvTranspose2d(
            512,
            256,
            kernel_size = (2,2),
            stride = (2,2),
            bias = False
        )

        self.biconvred2 = BiConvRedution( 512, 256 )
        self.up3 = nn.ConvTranspose2d(
            256,
            128,
            kernel_size = (2,2),
            stride = (2,2),
            bias = False
        )

        self.biconvred3 = BiConvRedution( 256, 128 )
        self.up4 = nn.ConvTranspose2d(
            128,
            64,
            kernel_size = (2,2),
            stride = (2,2),
            bias = False
        )

        self.biconvred4 = BiConvRedution( 128, 64 )
        self.conv_final = nn.Conv2d(
            64,
            out_channels,
            kernel_size = (1,1),
            bias = False
        )


    def forward( self, x ) :

        # Contracting
        out = self.conv1( x )
        s1 = self.biconv1( out )
        out = self.max1( s1 )

        out = self.conv2( out )
        s2 = self.biconv2( out )
        out = self.max2( s2 )

        out = self.conv3( out )
        s3 = self.biconv3( out )
        out = self.max3( s3 )

        out = self.conv4( out )
        s4 = self.biconv4( out )
        out = self.max4( s4 )


        # Mid
        out = self.conv_mid( out )
        out = self.biconv_mid( out )
        out = self.up1( out )


        # Expand
        out = torch.cat([out, s4], 1)
        out = self.biconvred1( out )
        out = self.up2( out )

        out = torch.cat([out,s3], 1)
        out = self.biconvred2( out )
        out = self.up3( out )

        out = torch.cat([out,s2], 1)
        out = self.biconvred3( out )
        out = self.up4( out )

        out = torch.cat([out,s1], 1)
        out = self.biconvred4( out )
        out = self.conv_final( out )

        return out