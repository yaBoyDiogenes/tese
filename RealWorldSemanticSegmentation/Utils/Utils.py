from PIL import Image
from torchvision import transforms

import numpy as np

def to_pil( x ) :

    return transforms.ToPILImage()(x.squeeze_(0))

def numpy_to_pil( x ) :
    return Image.fromarray( np.uint( x ) * 255 )