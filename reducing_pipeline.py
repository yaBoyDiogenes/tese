from torch import nn

from torchvision import transforms
from torch.utils.data import DataLoader
from RealWorldSemanticSegmentation.Models.UNet import UNet
from RealWorldSemanticSegmentation.Datasets.COCO_utils import COCO

import torch
import torch.nn.functional as F
import torch.nn.utils.prune as prune


base = '/home/luispereira/Desktop/COCOdataset2017'
classes = [
    'person',
    'bicycle',
    'car',
    'bus',
    'bird',
    'cat',
    'dog',
    'bottle',
    'cup',
    'laptop',
]

model_name = 'mod16.pt'


device = torch.device(
    'cuda' if torch.cuda.is_available() else 'cpu'
)

model = UNet( 3, len( classes ) + 1 ).to( device )
model.load_state_dict( torch.load( model_name ) )
model.eval()



t = transforms.Compose([
    transforms.ToTensor()
])

val = COCO(
    dir = base,
    classes = classes,
    mode = 'val',
    transform = t
)

val_dl = DataLoader(
    val,
    batch_size=8,
    shuffle=True,
)


'''
print(len(val_dl) * 32)


'''



from time import time

def vv( model, gpu=True ) :
    with torch.no_grad() :
        
        x = time()

        for image, lbl in val_dl :
            if gpu :
                image = image.to( device )
            output = model( image )
            break

        print( time() - x )


# magnitude prunning

vv ( model )

print( model.conv1.weight.nelement() )

parameters_to_prune = (
    (model.conv1 ,'weight'),
    (model.conv2 ,'weight'),
    (model.conv3 ,'weight'),
    (model.conv4 ,'weight'),
    (model.biconvred1.conv1, 'weight'),
    (model.biconvred1.conv2, 'weight'),
    (model.biconvred2.conv1, 'weight'),
    (model.biconvred2.conv2, 'weight'),
    (model.biconvred3.conv1, 'weight'),
    (model.biconvred3.conv2, 'weight'),
    (model.biconvred4.conv1, 'weight'),
    (model.biconvred4.conv2, 'weight'), 
)

prune.global_unstructured(
    parameters_to_prune,
    pruning_method=prune.L1Unstructured,
    amount=0.4
)

prune.remove( model.conv1, 'weight' )
prune.remove( model.conv2, 'weight' )
prune.remove( model.conv3, 'weight' )
prune.remove( model.conv4, 'weight' )
prune.remove(model.biconvred1.conv1, 'weight'),
prune.remove(model.biconvred1.conv2, 'weight'),
prune.remove(model.biconvred2.conv1, 'weight'),
prune.remove(model.biconvred2.conv2, 'weight'),
prune.remove(model.biconvred3.conv1, 'weight'),
prune.remove(model.biconvred3.conv2, 'weight'),
prune.remove(model.biconvred4.conv1, 'weight'),
prune.remove(model.biconvred4.conv2, 'weight'), 
vv ( model )


# prune.remove(model.conv1, 'weight')
# prune.remove(model.conv2, 'weight')
# prune.remove(model.conv3, 'weight')
# prune.remove(model.conv4, 'weight')
# prune.remove(model.conv_final, 'weight')
# prune.remove(model.conv_mid, 'weight')




print( model.conv1.weight.nelement() )











# model = UNet( 3, len( classes ) + 1 ).to( device )
# model.load_state_dict( torch.load( model_name ) )
# model.eval()

nw = model.to( 'cpu' )
q = torch.quantization.quantize_dynamic(
    nw, {
        nw.conv1,
        nw.conv2,
        nw.conv3,
        nw.conv4,
        nw.biconvred1.conv1,
        nw.biconvred1.conv2,
        nw.biconvred2.conv1,
        nw.biconvred2.conv2,
        nw.biconvred3.conv1,
        nw.biconvred3.conv2,
        nw.biconvred4.conv1,
        nw.biconvred4.conv2,
    }, dtype=torch.qint8 
)

q = q.cuda()
vv( q )

'''

nw = model.to( 'cpu' )

# FUSE HERE
torch.quantization.fuse_modules( nw, [nw.biconvred1.conv1, nw.biconvred1.bn1], inplace=True)


nw = torch.quantization.default_qconfig
torch.quantization.prepare( nw, inplace=True )

# calibrate, using eval


torch.quantization.convert( nw, inplace=True )

'''










torch.save(
    model.state_dict(),
    'new.pt'
)

torch.save(
    q.state_dict(),
    'new_quanti.pt'
)